app.factory('Accordion', function(){
    return {
        accordion: function() {
            function toggleChevron(e) {
                $(e.target)
                        .prev('.panel-heading')
                        .find("i.indicator")
                        .toggleClass('fa fa-plus-square-o fa fa-minus-square-o');
            }
            $('.accordion').on('hidden.bs.collapse', toggleChevron);
            $('.accordion').on('shown.bs.collapse', toggleChevron);
        }
    
    }
});