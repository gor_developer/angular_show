app.factory('Page', function(){
  var title = 'Intop';
  var metaDescription = 'Intop, get your files';
  var metaKeywords = 'Intop, files';
  return {
    title: function() { return title; },
    setTitle: function(newTitle) { title = newTitle; },
    metaDescription: function() { return metaDescription; },
    setMetaDescription: function(newMetaDescription) { metaDescription = newMetaDescription; },
    metaKeywords: function() { return metaKeywords; },
    setMetaKeywords: function(newMetaKeywords) { metaKeywords = newMetaKeywords; }
  };
});