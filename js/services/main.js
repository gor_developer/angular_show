app.factory('MainService', function($http) {
  return {
    getAuthors: function() {
        var req = {
            method: 'GET',
            url: base_url + 'resources/distinct/author',
            headers: {
              'Content-Type': "application/json"
            },
        }
        return $http(req).then(function (results) {
            return results;
        });
    },
  };
});