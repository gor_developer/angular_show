app.factory('LoginService', function($http) {
  return {
    loginUser: function(user) {
        var json_data = angular.toJson(user);
        var req = {
        method: 'POST',
        url: base_url + 'authenticate/login',
        headers: {
          'Content-Type': "application/json"
        },
        data: json_data
//        data: 'username'+ user.username + '&password' + user.password
       }
        return $http(req).then(function (results) {
            return results;
        });
    },
    
  };
});