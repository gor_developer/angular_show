app.factory('DataTable', function($timeout, $filter, $rootScope){
    var table;
    return {
        dataTable: function() {
            $timeout(function() {
                var params = '';
                var i = 0;
                var count = 0;
                for (var k in $rootScope.params) {
                    if ($rootScope.params[k] != 0) {
                       ++count;
                    }
                }
                angular.forEach($rootScope.params, function(value, key) {
                    if(value != '') {
                        if(i == 0) {
                            params += '?'
                        }
                        params += key + '=' + value;
                        if(i < count - 1) {
                            params += '&'
                        }
                        i++;
                    }
                });
                table = $('#files_table').DataTable({
                    "processing": true,
                    "serverSide": true,
//                    "sAjaxSource": base_url + "resources" + params,
                    "ajax": {
                        "method": 'GET',
                        "url":base_url + "resources" + params,
                        headers: {
                            'Content-Type': "application/json"
                        },
                        "data": function(d) {
                            d.from = d.start;
                            d.count = d.length;
                        },
                        dataFilter: function(data){
                            var json = jQuery.parseJSON(data);
                            json.recordsTotal = json.count;
                            json.recordsFiltered = json.count;
                            json.data = json.result;
                            $rootScope.tableReady = true;
                            $rootScope.$apply();
                            return JSON.stringify(json);
                        }
                    },
                    
                    "columns": [
                        {
                            "data": null,
                            "render": function (data) {
                                var url = data.url;
                                var to = url.lastIndexOf('/');
                                to = to == -1 ? url.length : to + 1;
                                url = url.substring(0, to);
                                return '<a href="' + url + '" target="_blank" class="table-link">' + data.name + '</a>'+
                                        '<p class="textAbstract" ng-show="showAbstractText">' + data.contentExcerpt + '</p>';
                            },
                            "name": "name"
                        },
                        {
                            "data": "modified",
                            "render": function (data) {
                                return $filter('date')(data, 'dd / MM / yyyy');
                            },
                            "name": "modified"
                        },
                        {
                            "data": "filetype",
                            "render": function (data) {
                                var class_name = '';
                                var text = '';
                                if (data == 'doc' || data == 'docx')  { class_name="w-icons word-icon"; }
                                else if (data == 'ppt' || data == 'pptx' || data == 'pptm') { class_name="w-icons ppt-icon"; }
                                else if (data == 'pdf') { class_name="w-icons pdf-icon"; }
                                else if (data == 'html') { class_name="w-icons html-icon"; }
                                else if (data == 'xlsx' || data == 'xls' || data == 'xlsm') { class_name="w-icons xls-icon"; }
                                else if (data == 'txt') { class_name="w-icons txt-icon"; }
                                else if (data == 'dat') { class_name="w-icons dat-icon"; }
                                else if (data == 'asc') { class_name="w-icons asc-icon"; }
                                else if (data == 'xyz') { class_name="w-icons xyz-icon"; }
                                else if (data == 'zip') { class_name="w-icons zip-icon"; }
                                else if (data == 'png') { class_name="w-icons png-icon"; }
                                else if (data == 'las') { class_name="w-icons las-icon"; }
                                else if (data == 'rtf') { class_name="w-icons rtf-icon"; }
                                else if (data == 'lnk') { class_name="w-icons lnk-icon"; }
                                else if (data != 'xlsx' && data != 'xls' && 
                                        data != 'html' && data != 'pdf' && 
                                        data != 'ppt' && data != 'pptx' && 
                                        data != 'doc' && data != 'docx' && 
                                        data != 'pptm' && data != 'xlsm' &&
                                        data != 'txt' && data != 'dat' &&
                                        data != 'asc' && data != 'xyz' &&
                                        data != 'zip' && data != 'png' &&
                                        data != 'las' && data != 'rtf' &&
                                        data != 'lnk') {
                                    class_name = "";
                                    if(typeof data != 'undefined') {
                                        text = "type - ." + data;
                                    } else {
                                        text = "type - .";
                                    }
                                }
                                return '<span class="' + class_name + '">' + text + '</span>';
                            },
                            "name": "filetype"
                        },
                        {
                            "data": "author",
                            "render": function (data) {
                                if(typeof data == 'undefined' || data.length == 0) {
                                    return '';
                                } else {
                                    return data;
                                }
                            },
                            "name": "author"
                        },
                        {
                            "data": null,
                            "render": function () {
                                return '<i class="icon-eye w-icons icons"></i>';
                            }
                        },
                        {
                            "data": "url",
                            "render": function (data) {
                                return '<a href="mailto:?subject=Shared document from IntOp&body=Link to document: /' + data + '/">'+
                                                '<i class="icon-share  w-icons icons"></i>'+
                                            '</a>';
                            }
                        },
                        {
                            "data": null,
                            "render": function () {
                                return '<i class="icon-note  w-icons icons"></i>';
                            }
                        },
                        {
                            "data": null,
                            "render": function ( data, type, full, meta ) {
                                return '';
                            }
                        },
                    ],
                    "order": [[ 0, "asc" ]],
                    "columnDefs": [
                        { "targets": [4,5,6,7], "orderable": false }
                    ],
                    language: {
                        paginate: {
                            next: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                            previous: '<i class="fa fa-angle-left" aria-hidden="true"></i>'  
                        }
                    },
//                    "iDisplayLength": 25,
//                    "aLengthMenu": [[25, 50, 100, 500], [25, 50, 100, 500]],
                    "lengthMenu": [25, 50, 100],
                    "dom": 'ltri<"texts"p>',
                });
                var ww = $(window).width();
                if (ww > 482) {
                    var texts = '<span class="show-pages">Pages</span>'+
                                '<span class="browse-pages">Browse pages</span>';
                    $('.texts').prepend(texts);
                }
            }, 0);
        },
        updateDataTable: function() {
//            table.destroy();
        }
    };
});