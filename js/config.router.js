'use strict';

/**
 * Config for the router
 */
angular.module('app')
  .run(
    ['$rootScope', '$state', '$stateParams', '$cookieStore', '$location', '$http',
      function ($rootScope, $state, $stateParams, $cookieStore, $location, $http) {
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;

            var currentUrl = $location.path();
            if(currentUrl != '/login' && currentUrl != '/logout') {
                var access_token = $cookieStore.get('access_token');
                if(typeof access_token != 'undefined') {
                    var currentTime = new Date().getTime();
                    var expires_in = $cookieStore.get('expires_in');
                    if(typeof expires_in != 'undefined' || expires_in > currentTime) {
//                        $http.defaults.headers.common['Content­Type'] = 'application/json';
                        $http.defaults.headers.common['authorization'] = 'bearer ' + access_token;
                    }
                }
            }
      }
    ]
  )
  .config(
    [          '$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', 'MODULE_CONFIG',
      function ($stateProvider, $urlRouterProvider, JQ_CONFIG, MODULE_CONFIG) {
 
          var base_url = "some-base-url";
          $urlRouterProvider
              .otherwise('/');
          
          $stateProvider
              .state('index', {
                  cache: false,
//                  abstract: true,
                  url: '/',
                  templateUrl: 'templates/index.html',
                  controller: 'HomeCtrl'
              })
              .state('login', {
                  cache: false,
//                  abstract: true,
                  url: '/login',
                  templateUrl: 'templates/login.html',
                  controller: 'LoginCtrl',
              })
              
              .state('logout', {
                url: '/logout',
                cache: false,
            //    abstract: true,
                controller: 'LogoutCtrl'
              })
              
          function load(srcs, callback) {
            return {
                deps: ['$ocLazyLoad', '$q',
                  function( $ocLazyLoad, $q ){
                    var deferred = $q.defer();
                    var promise  = false;
                    srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
                    if(!promise){
                      promise = deferred.promise;
                    }
                    angular.forEach(srcs, function(src) {
                      promise = promise.then( function(){
                        if(JQ_CONFIG[src]){
                          return $ocLazyLoad.load(JQ_CONFIG[src]);
                        }
                        angular.forEach(MODULE_CONFIG, function(module) {
                          if( module.name == src){
                            name = module.name;
                          }else{
                            name = src;
                          }
                        });
                        return $ocLazyLoad.load(name);
                      } );
                    });
                    deferred.resolve();
                    return callback ? promise.then(function(){ return callback(); }) : promise;
                }]
            }
          }


      }
    ]
  );
