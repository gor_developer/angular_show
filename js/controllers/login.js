app.controller('LoginCtrl',function($scope, LoginService, $state, Page, $cookieStore, $location)
{
    var access_token = $cookieStore.get('access_token');
    var expires_in = $cookieStore.get('expires_in');
    if(typeof access_token != 'undefined') {
        var currentTime = new Date().getTime();
        if(expires_in > currentTime) {
            $state.go('index');
        } else {
            $cookieStore.remove('access_token');
            $cookieStore.remove('expires_in');
        }
    } else if(typeof expires_in != 'undefined') {
        $cookieStore.remove('expires_in');
    }
    
    Page.setTitle('Login | Intop');
    Page.setMetaDescription('Login to use Intop application.');
    Page.setMetaKeywords('Intop, login');
    
    $scope.user = {};
    $scope.loginUser = function() {
        LoginService.loginUser($scope.user).then(function (results) {
            if(results.statusText == 'OK') {
                $cookieStore.put('access_token',results.data.access_token);
                $cookieStore.put('expires_in',results.data.expires_in);
                $state.go("index");
            } else {
                alert("Wrong user credentials");return;
            }
        });
    }
});

app.controller('LogoutCtrl',function($scope, LoginService, $state, Page, $cookieStore, $location)
{
    $cookieStore.remove('access_token');
    $cookieStore.remove('expires_in');
    $state.go("login");
});
