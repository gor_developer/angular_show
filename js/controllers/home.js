app.controller('HomeCtrl',function($scope, Page, MainService, DataTable, $timeout, Accordion, $rootScope)
{
    $rootScope.params = {
        author: '',
    };
    Page.setTitle('Intop');
    Page.setMetaDescription('Welcome to our Intop application.');
    Page.setMetaKeywords('Intop, Files');
    Accordion.accordion();
    $scope.files = {};
    $scope.allAuthors = {};
    $scope.committees = {};
    $scope.licenses = {};
    $scope.organizations = {};
    $scope.wells = {};
    $scope.disciplines = {};
    $scope.locations = {};
    $scope.meetingTypes = {};
    $scope.fileTypes = {};
    $scope.sources = {};
    $rootScope.tableReady = false;
    $scope.showAbstractText = true;
    var abstractTextShow = true;
    $scope.abstractText = function() {         // ng-show doesn't work from datatables render, so added this function
        if(abstractTextShow == true) {
            $('.textAbstract').addClass('hide');
            abstractTextShow = false;
        } else {
            $('.textAbstract').removeClass('hide');
            abstractTextShow = true;
        }
    };
    DataTable.dataTable();
    MainService.getAuthors().then(function (results) {
        if(results.status == 200) {
            $scope.allAuthors = results.data;
        }
    });
    
});
