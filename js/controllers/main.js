app.controller('MainCtrl',function($scope, Page, $cookieStore, $state, $location)
{
//    $scope.Page = Page;
    var currentUrl = $location.path();
    if(currentUrl != '/login') {
        var access_token = $cookieStore.get('access_token');
        if(typeof access_token == 'undefined') {
            $state.go('login');
        } else {
            var currentTime = new Date().getTime();
            var expires_in = $cookieStore.get('expires_in');
            if(typeof expires_in == 'undefined' || expires_in <= currentTime) {
                $cookieStore.remove('access_token');
                $state.go('login');
            }
        }
    }
});
