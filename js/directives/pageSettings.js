app.directive('pageSettings', function ($compile) {
    return {
        restrict: 'EAC',
        templateUrl: 'templates/directives/page-settings.html',
        link: function(scope, element, attrs) {
            $('.popswitch').popover({ // Popover Settings
                trigger: "click", 
                html: true,
                content: function(){
                    return $compile($("#settings_block").html())(scope);
                }  
            });
            $(document).mouseup(function(e) {
                var container = $(".popover");
                if (!container.is(e.target) // if the target of the click isn't the container...
                    && container.has(e.target).length === 0) // ... nor a descendant of the container
                {
                    container.popover("hide");
                }
            });
        }
    };
});