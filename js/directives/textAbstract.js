app.directive('fileLocation', function ($compile) {
    return {
        restrict: 'A',
        scope: {
            filelink: '@filelink',
        },
        link: function(scope, element, attrs) {
            var url = scope.filelink;
            var to = url.lastIndexOf('/');
            to = to == -1 ? url.length : to + 1;
            url = url.substring(0, to);
            element[0].setAttribute('href', url);
        }
    };
});